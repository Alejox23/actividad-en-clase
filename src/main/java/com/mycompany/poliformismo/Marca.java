package com.mycompany.poliformismo;
public class Marca extends Celular{
    protected String fabricante;
    public Marca(String nombre, double precio, String fabricante){
        super(nombre, precio);
        this.fabricante=fabricante;
    }

    public String getFabricante() {
        return fabricante;
    }
    @Override
    public String imprimir(){
        return "nombre del producto: " + nombre +"\nprecio dle producto: "+ precio + "\nnombre del fabricante: " + fabricante;
    }
}
